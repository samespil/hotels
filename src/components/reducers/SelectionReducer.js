import * as actionTypes from '../actions/types';

export default (state = null, action) => {
	switch(action.type) {
		case actionTypes.SELECT_HOTEL:
			return action.payload;
		default:
			return state;
	}
};