import { combineReducers } from 'redux';
import HotelReducer from './HotelReducer';
import SearchReducer from './SearchReducer';

export default combineReducers({
	hotels: HotelReducer,
	searchText: SearchReducer
});
