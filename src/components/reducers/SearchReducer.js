import * as actionTypes from '../actions/types';

const INITIAL_STATE = { searchText: '' };

export default (state = INITIAL_STATE, action) => {
	switch(action.type) {
		case actionTypes.SEARCH_TEXT_CHANGED:
			return { ...state, searchText: action.payload};
		default:
			return state;
	}
};