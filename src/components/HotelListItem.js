import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity  } from 'react-native';
import { Card, CardSection, Button } from './common';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import Stars from './Stars';
import MealPlanPicker from './MealPlanPicker';

class HotelListItem extends Component {

	render () {
		return (
			<TouchableOpacity onPress={() => Actions.hotelDetail( {hotel: this.props.hotel })}>
				<Card>
					<CardSection>
						<View>
							<Image source={{ uri: this.props.hotel.image }} style={styles.imageStyle} />
						</View>
					<CardSection>
					</CardSection>
						<View>
							<Text style={styles.titleStyle}>{this.props.hotel.name}</Text>
						</View>
					</CardSection>
					<CardSection>
						<View style={styles.itemFooterStyle}>
							<CardSection style={ {flex: 1} }>
								<Stars number={this.props.hotel.starRating} color={"yellow"} />
							</CardSection>
							<CardSection style={ {flex: 10} }>
								<Text>precio por noche</Text>
								<Text style={styles.priceStyle}>A$R {this.props.hotel.price}</Text>
							</CardSection>
						</View>
					</CardSection>
				</Card>
			</TouchableOpacity>
			);
		}
}

const styles = {
	titleStyle: {
		flexDirection: 'row',
		justifyContent: 'space-around',
		fontSize: 24,
		fontWeight: '400'
	},
	priceStyle: {
		fontSize: 30,
		fontWeight: '600'
	},
	imageStyle: {
        height: 150,
        width: null,
        flex: 1,
		resizeMode: 'cover', // or 'stretch'
	},
	itemFooterStyle: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'flex-start',
		marginLeft: 10,
		marginRight: 10
	}
};

export default HotelListItem;