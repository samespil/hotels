import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, ListView, ScrollView, UIManager, LayoutAnimation, TouchableOpacity } from 'react-native';
import * as actions from './actions';
import Icon from 'react-native-vector-icons/FontAwesome';

class MealPlanPicker extends Component {

	state = { mealPlans: [], expanded: false, selectedMeal: null };

	componentWillUpdate() {
		UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
		LayoutAnimation.spring();
	}

	componentWillMount() {
		this.setState({ mealPlans: this.props.mealPlans, expanded: this.props.expanded, selectedMeal: this.props.selectedMeal })

		const ds = new ListView.DataSource({
			rowHasChanged: (r1, r2) => r1 !== r2	
		});
		
		this.dataSource = ds.cloneWithRows(this.props.mealPlans);
	}

	onMealPlanSelect(mealOption) {
		this.setState({ expanded: false, selectedMeal: mealOption });
		this.props.mealPlanChanged(mealOption.id);
	}
	
	renderItem(mealOption) {
		if(mealOption === this.state.selectedMeal) {
			return (
				<TouchableOpacity key={mealOption.id} onPress={() => this.onMealPlanSelect(mealOption)} style={styles.pickerStyle}>
					<Icon style={styles.optionIconStyle} name={mealOption.icon}/>
					<Text style={styles.optionTextStyle}>{mealOption.label}</Text>
					<Icon style={styles.optionIconStyle} name="check" /> 
				</TouchableOpacity>
			);
		} else {
			return (
				<TouchableOpacity key={mealOption.id} onPress={() => this.onMealPlanSelect(mealOption)} style={styles.pickerStyle}>
					<Icon style={styles.optionIconStyle} name={mealOption.icon}/>
					<Text style={styles.optionTextStyle}>{mealOption.label}</Text>
				</TouchableOpacity>
			);
		}
	}

	render () {
		if(this.state.expanded === true) {
			return (
				<ListView 
					dataSource={this.dataSource}
					renderRow={this.renderItem.bind(this)}
				/>
			);
		} else {
			return (
				<ScrollView>
					<TouchableOpacity onPress={() => this.setState({ expanded: true })} style={styles.pickerStyle}>
						<Icon style={styles.optionIconStyle} name={this.state.selectedMeal.icon} />
						<Text style={styles.optionTextStyle}>{this.state.selectedMeal.label}</Text>
						<Icon style={styles.optionIconStyle} name="caret-down" />
					</TouchableOpacity>
				</ScrollView>
			);
		}
	}
}

const styles = {
	optionIconStyle: {
		flex: 1,
		paddingLeft: 20,
		paddingRight: 20,
		alignSelf: 'center',
		color: '#007aff',
		fontSize: 16,
		fontWeight: '600',
		marginTop: 5,
		marginBottom: 5
	},
	optionTextStyle: {
		flex: 12,
		paddingLeft: 20,
		paddingRight: 20,
		alignSelf: 'center',
		color: '#007aff',
		fontSize: 16,
		fontWeight: '600',
		marginTop: 5,
		marginBottom: 5
	},
	pickerStyle: {
		flexDirection: 'row'
	}

};

export default connect(null, actions)(MealPlanPicker);