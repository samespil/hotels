import * as actionTypes from './types';

export const searchTextChanged = (searchText) => {

	return {
		type: actionTypes.SEARCH_TEXT_CHANGED,
		payload: searchText
	};
};

export const mealPlanChanged = (mealPlanId) => {

	console.log('mealPlanChanged -- mealPlanId: ' + mealPlanId);
	return {
		type: actionTypes.MEAL_PLAN_CHANGED,
		payload: mealPlanId
	};

};