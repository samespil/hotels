import React, { Component } from 'react';
import { View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

class Stars extends Component {

	render(){
		var stars = [];
		for(let i = 0; i < this.props.number; i++){
			stars.push(<Icon key={i} name="star" size={20} color={this.props.color} style={styles.iconStyle} />)
		}
		
		return (
			<View style={styles.containerStyle}>
				{ stars }
			</View>
		)
	}
}

const styles = {
	iconStyle: {
		paddingLeft: 5,
		paddingRight: 5,
		alignSelf: 'center',
		marginTop: 5,
		marginBottom: 5
	},
	containerStyle: {
		flexDirection: 'row'
	}
};

export default Stars;