import React, { Component } from 'react';
import { ScrollView, ListView } from 'react-native';
import HotelListItem from './HotelListItem';
import { connect } from 'react-redux';
import * as actions from './actions';
import { TextField } from './common';

class HotelList extends Component {

  state = { searchText: '', hotels: [] };

	componentWillMount() {
		this.setState({ searchText: '', hotels: this.props.hotels })

		const ds = new ListView.DataSource({
			rowHasChanged: (r1, r2) => r1 !== r2	
		});

		this.dataSource = ds.cloneWithRows(this.props.hotels);
	}

	onSearchTextChange(searchText) {
		this.props.searchTextChanged(searchText);
	}

	renderItem(hotel) {
		return <HotelListItem key={hotel.id} hotel={hotel} />;
	}

	render () {
		return ( 
 			<ScrollView>
				<TextField 
					label={'Buscar'} 
					placeholder={'Buscar'}
					value={this.state.searchText}
					onChangeText={searchText => this.setState({ searchText })}
				/>
				<ListView 
					dataSource={this.dataSource}
					renderRow={this.renderItem}
				/>
			</ScrollView>
		);
	}
}

const mapStateToProps = state => {
	return { 
		hotels: state.hotels,
		searchText: state.searchText
	};
};

export default connect(mapStateToProps, actions)(HotelList);