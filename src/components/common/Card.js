import React from 'react';
import { View } from 'react-native';

const Card = (props) => {
	return ( 
		<View style={styles.containerStyle}>
			{props.children}
		</View>
	);
};

const styles = {
	containerStyle: {
		borderWidth: 0,
		borderRadius: 5,
		shadowColor: '#000',
		shadowOffset: { with: 0, height: 5 },
		shadowOpacity: 0.2,
		shadowRadius: 2,
		elevation: 1,
		marginLeft: 5,
		marginRight: 5,
		marginTop: 10
	},
};

export { Card };