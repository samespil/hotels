import React from 'react';
import { View, Text, TextInput } from 'react-native';

const TextField = ( {label, value, onChangeText, placeholder} ) => {

	return ( 
		<View style={styles.containerStyle}>
			<Text style={styles.labelStyle}>{label}</Text>
			<TextInput 
				value={value}
				onChangeText={onChangeText}
				placeholder={placeholder}
				style={styles.inputStyle}
				autoCorrect={false}
			/>
		</View>
	);
};

const styles = {
	inputStyle: {
		height: 40,
		width: 100,
		color: '#000',
		paddingRight: 5,
		paddingLeft: 5,
		fontSize: 18,
		lineHeight: 23,
		flex: 2
	},
	labelStyle: {
		fontSize: 18,
		paddingLeft: 20,
		flex: 1
	},
	containerStyle: {
		marginTop: 20,
		marginBottom: 10,
		flexDirection: 'row',
		alignItems: 'center'
	}
};

export { TextField };