import React from 'react';
import { View, Text } from 'react-native';

const Header = (props) => {
	const { viewStyle, textStyle } = styles;

	return ( 
		<View style={viewStyle}>
			<Text style={textStyle}>{props.headerTitle}</Text>
		</View>
		);
};

const styles = {
	textStyle: {
		fontSize: 20
	},
	viewStyle: {
		backgroundColor: '#F2F2F2',
		justifyContent: 'center',
		alignItems: 'center',
		height: 60,
		paddingTop: 15,
		shadowColor: '#000',
		shadowOffset: { with: 0, height: 200 },
		shadowOpacity: 0.2,
		elevation: 2,
		position: 'relative'
	}
};

export { Header };