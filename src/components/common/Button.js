import React, { Component } from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = (props) => {

	return ( 
		<TouchableOpacity onPress={props.onPress} style={styles.buttonStyle}>
			<Text style={styles.buttonTextStyle}>{props.buttonText}</Text>
		</TouchableOpacity>
	);
};

const styles = {
	buttonStyle: {
		flex: 1,
		alignSelf: 'stretch',
		backgroundColor: '#fff',
		borderRadius: 5,
		borderWidth: 2,
		borderColor: '#007aff',
		marginLeft: 20,
		marginRight: 20
	},
	buttonTextStyle: {
		alignSelf: 'center',
		color: '#007aff',
		fontSize: 16,
		fontWeight: '600',
		marginTop: 5,
		marginBottom: 5
	}
};

export { Button };