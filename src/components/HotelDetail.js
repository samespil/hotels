import React, { Component } from 'react';
import { View, ScrollView, Text, Image, StyleSheet } from 'react-native';
import { Card, CardSection } from './common';
import MealPlanPicker from './MealPlanPicker';
import Stars from './Stars';
import MapView from 'react-native-maps';
import Icon from 'react-native-vector-icons/FontAwesome';

class HotelDetail extends Component {

	renderAmenitiesIcons(amenitiesIcons){
		var icons = [];
		for(let i = 0; i < amenitiesIcons.length; i++){
			icons.push(<Icon key={i} name={amenitiesIcons[i]} size={40} color="#fff" style={styles.iconStyle} />)
		}
		
		return (
			<View style={styles.amenitiesSectionStyle}>
				{ icons }
			</View>
		)
	}

	render () {
		return (
 			<ScrollView>
				<View style={styles.containerStyle}>
					<View style={styles.imageContainerStyle}>
						<Image source={{ uri: this.props.hotel.image }} style={styles.imageStyle} />
					</View>
					<View style={styles.titleContainerStyle}>
						<View>
							<Text style={styles.titleStyle}>{this.props.hotel.name}</Text>
							<Stars style={styles.ratingStyle} number={this.props.hotel.starRating} color={"yellow"} />
							<View style={styles.addressStyle}>
								<Icon name="map-marker" size={25} style={styles.iconStyle}/>
								<Text>{this.props.hotel.address}</Text>
							</View>
						</View>
						<View style={styles.mapContainerStyle}>
							<MapView
								initialRegion={
									this.props.hotel.location
								}
								style={styles.map}
								customMapStyle={mapStyle}
							/>
						</View>
					</View>
					<View style={styles.sectionStyle}>
						<View style={styles.sectionContentStyle}>
							<Text style={styles.sectionTitleTextStyle}>Opiniones</Text>
							<Text style={ {flex: 1} }>{this.props.hotel.totalComments} opiniones</Text>
						</View>
					</View>
					{this.renderAmenitiesIcons(this.props.hotel.amenitiesLogo)}
					<View style={styles.sectionStyle}>
						<View style={styles.sectionContentStyle}>
							<Text style={styles.sectionTitleTextStyle}>Habitaciones</Text>
						</View>
						<View style={styles.sectionContentStyle}>
							<MealPlanPicker 
								mealPlans={this.props.hotel.mealPlans} 
								selectedMeal={this.props.hotel.mealPlans[0]}
							/>
						</View>
						<View style={styles.sectionContentStyle}>
							<Text >Precio por noche por habitacion</Text>
							<Text style={ {fontSize: 24}}>  ARS  </Text>
							<Text style={styles.priceStyle}>{this.props.hotel.price}</Text>
						</View>
					</View>
					<View style={styles.sectionStyle}>
						<View style={styles.sectionContentStyle}>
							<Text style={styles.sectionTitleTextStyle}>Descripcion</Text>
						</View>
						<View style={styles.sectionContentStyle}>
							<View style={styles.sectionContentStyle}>
								<Icon name="clock-o" size={20} color="#000" style={styles.iconStyle} />
							</View>
							<View style={{ flexDirection: 'column' }}>
								<Text>Check-in desde: {this.props.hotel.checkInFrom} hs</Text>
								<Text>Check-out hasta: {this.props.hotel.checkOutUntil} hs</Text>
							</View>
						</View>
						<View style={styles.sectionContentStyle}>
							<Text>{this.props.hotel.locationDetails}</Text>
						</View>
					</View>
					<View style={styles.sectionStyle}>
						<View style={styles.sectionContentStyle}>
							<Text style={styles.sectionTitleTextStyle}>{this.props.hotel.conditions[0].title}</Text>
						</View>
						<View style={styles.sectionContentStyle}>
							<Text>{this.props.hotel.conditions[0].text}</Text>
						</View>
						<View style={styles.sectionContentStyle}>
							<Text style={styles.sectionTitleTextStyle}>{this.props.hotel.conditions[1].title}</Text>
						</View>
						<View style={styles.sectionContentStyle}>
							<Text>{this.props.hotel.conditions[1].text}</Text>
						</View>
						<View style={styles.sectionContentStyle}>
							<Text style={styles.sectionTitleTextStyle}>{this.props.hotel.conditions[2].title}</Text>
						</View>
						<View style={styles.sectionContentStyle}>
							<Text>{this.props.hotel.conditions[2].text}</Text>
						</View>
					</View>
				</View>
 			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	containerStyle: {
	},
	imageStyle: {
        alignSelf: 'stretch',
		resizeMode: 'cover', // or 'stretch'
        height: 300,
        width: null,
        flex: 1,
	},
	titleContainerStyle: {
		flexDirection: 'column',
		justifyContent: 'space-around',
		backgroundColor: '#fff',
		marginLeft: 10,
		marginRight: 10,
		borderRadius: 3,
		shadowColor: '#000',
		shadowOffset: { with: 0, height: 5 },
		shadowOpacity: 0.2,
		transform: [
		  { translateY: -30},

		],
		zIndex: 10
	},
	titleStyle: {
		fontSize: 24,
		fontWeight: '600',
		color: '#000',
		paddingLeft: 10,
		paddingRight: 10,
		paddingTop: 5,
		paddingBottom: 15,
	},
	ratingStyle: {
		paddingLeft: 10,
		paddingRight: 10,
		paddingTop: 5,
		paddingBottom: 15,
	},
	addressStyle: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingLeft: 10,
		paddingRight: 10,
		paddingTop: 5,
		paddingBottom: 15,
		marginRight: 20
	},
	iconStyle: {
		marginLeft: 15,
		marginRight: 15
	},
	imageContainerStyle: {
		justifyContent: 'center',
		alignItems: 'center',
        height: 200,
		zIndex: 0
	},
	mapContainerStyle: {
		justifyContent: 'center',
		alignItems: 'center',
        height: 150
	},
	map: {
	...StyleSheet.absoluteFillObject,
	},
	amenitiesSectionStyle: {
		flexDirection: 'row',
		justifyContent: 'space-around',
		backgroundColor: '#27427F',
		paddingTop: 20,
		paddingBottom: 20,
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 15,
		borderRadius: 3,
		shadowColor: '#000',
		shadowOffset: { with: 0, height: 5 },
		shadowOpacity: 0.2
	},
	sectionStyle: {
		flexDirection: 'column',
		justifyContent: 'space-around',
		backgroundColor: '#fff',
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 15,
		borderRadius: 3,
		shadowColor: '#000',
		shadowOffset: { with: 0, height: 5 },
		shadowOpacity: 0.2
	},
	sectionContentStyle: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingLeft: 3,
		paddingRight: 3
	},
	sectionTitleTextStyle: {
		fontSize: 18,
		fontWeight: '400',
		color: '#000',
		paddingLeft: 10,
		paddingRight: 10,
		paddingTop: 5,
		paddingBottom: 5,
		borderBottomWidth: 1,
		borderColor: '#ddd',
		flex: 2
	},
	priceStyle: {
		fontSize: 24,
		fontWeight: '500'
	},
});

const mapStyle = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#1d2c4d"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#8ec3b9"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1a3646"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#4b6878"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#64779e"
      }
    ]
  },
  {
    "featureType": "administrative.province",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#4b6878"
      }
    ]
  },
  {
    "featureType": "landscape.man_made",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#334e87"
      }
    ]
  },
  {
    "featureType": "landscape.natural",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#023e58"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#283d6a"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#6f9ba5"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1d2c4d"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#023e58"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#3C7680"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#304a7d"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#98a5be"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1d2c4d"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#2c6675"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#255763"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#b0d5ce"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#023e58"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#98a5be"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1d2c4d"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#283d6a"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#3a4762"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#0e1626"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#4e6d70"
      }
    ]
  }
];

export default HotelDetail;