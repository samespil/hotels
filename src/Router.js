import React from 'react';
import { Router, Stack, Scene } from 'react-native-router-flux';
import HotelList from './components/HotelList';
import HotelDetail from './components/HotelDetail';

const RouterComponent = () => {

	return (
		<Router>
			<Stack key="root">
				<Scene key="hotelList" component={HotelList} title="Lista Hoteles" initial />
				<Scene key="hotelDetail" component={HotelDetail} title="Hotel" />
			</Stack>
		</Router>
	);
};

export default RouterComponent;